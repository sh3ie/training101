*** Settings ***

*** Keywords ***
LOGIN - Login to Facebook
    [Arguments]     ${username}     ${password}
    Log         username: ${username}, password: ${password}
    [Return]        Success