*** Settings ***
Library        String
Library        RequestsLibrary
Variables      ${CURDIR}/../resources/configs/common_configs.yaml
Variables      ${CURDIR}/../resources/configs/${ENV}/configs.yaml

*** Keywords ***
GET USER - GET User by User Id
    [Arguments]     ${user_id}
    ${path}         Replace String      ${URIs.GET_USER_BY_USER_ID}     {user_id}       ${user_id}
    ${headers}          Create Dictionary       Content-Type=application/json
    Create Session      get_user        ${DUMMY_API_HOST}     headers=${headers}
    ${response}     GET on Session      get_user        ${path}
    [Return]        ${response}

GET USER - Verify Response Data
    [Arguments]     ${response}     ${expect_status_code}       ${expect_email}     ${expect_firstname}
    ...     ${expect_lastname}      ${expect_id}        ${expect_avatar}
    Should Be Equal As Strings      ${response.status_code}                         ${expect_status_code}
    Should Be Equal As Strings      ${response.json()['data']['email']}             ${expect_email}
    Should Be Equal As Strings      ${response.json()['data']['first_name']}        ${expect_firstname}
    Should Be Equal As Strings      ${response.json()['data']['last_name']}         ${expect_lastname}
    Should Be Equal As Strings      ${response.json()['data']['id']}                ${expect_id}
    Should Be Equal As Strings      ${response.json()['data']['avatar']}            ${expect_avatar}