*** Settings ***

*** Variables ***
${username_admin}         admin

*** Test Cases ***
Login success
    [Tags]      smoke_test      integration_test        daily
    Log      ${username}
    ${response}         Login to server         ${username_admin}
    ...     1234567
    ${response}         Login to server with ${username_admin} and 1234567

*** Keywords ***
Login to server
    [Arguments]      ${username}        ${password}
    Log         username: ${username}, password: ${password}
    [Return]        Success

Login to server with ${username} and ${password}
    Log         username: ${username}, password: ${password}
    [Return]        Success

