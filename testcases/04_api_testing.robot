*** Settings ***
Variables      ${CURDIR}/../resources/testdata/common_testdata.yaml
Variables      ${CURDIR}/../resources/testdata/${ENV}/user_resources.yaml
Resource       ${CURDIR}/../keywords/get_user_keywords.robot

*** Test Cases ***
Get User by User Id - Success
    ${response}     GET USER - GET User by User Id      ${get_user.user_id}
    GET USER - Verify Response Data     ${response}         ${http_status.SUCCESS}      ${get_user.email}
    ...    ${get_user.firstname}       ${get_user.lastname}    ${get_user.user_id}          ${get_user.avatar}

API Testing
    ${user_id}          Set Variable        2

    ${headers}          Create Dictionary       Content-Type=application/json
    Create Session      get_user        ${host}     headers=${headers}
    ${response}     GET on Session      get_user        ${path}${user_id}

    # get response http status (200,400 or etc)
    Log         ${response.status_code}

    # get response body in dictionary format
    Log         ${response.json()}

    # get response body in raw text (no format)
    Log         ${response.text}

    # get response headers (dictionary)
    Log         ${response.headers}

    Should Be Equal As Strings      ${response.status_code}                         200
    Should Be Equal As Strings      ${response.json()['data']['email']}             janet.weaver@reqres.in
    Should Be Equal As Strings      ${response.json()['data']['first_name']}        Janet
    Should Be Equal As Strings      ${response.json()['data']['last_name']}         Weaver
    Should Be Equal As Strings      ${response.json()['data']['id']}                2
    Should Be Equal As Strings      ${response.json()['data']['avatar']}            https://reqres.in/img/faces/2-image.jpg


