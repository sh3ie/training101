*** Settings ***
Resource        ${CURDIR}/../keywords/login_keywords.robot
Variables       ${CURDIR}/../resources/testdata/common_testdata.yaml
Variables       ${CURDIR}/../resources/testdata/${ENV}/login_resource.yaml

*** Test Cases ***
Login to Facebook - Success
    Log         ${login_success['username']}
    Log         ${login_success['password']}
    Log         ${ENV}
    LOGIN - Login to Facebook       ${login_success['username']}       ${login_success['password']}
