*** Settings ***

*** Variables ***
${http_response_code}           200

*** Test Cases ***
Test conditions
    #old syntax
    Run Keyword If      ${http_response_code} == 200
    ...         Log       Success
    ...         ELSE
    ...         Log       Fail

    #new syntax
    IF      ${http_response_code} == 200
        Log       Success
    ELSE
        Log       Fail
    END

Test Loops
    #create list
    ${list}         Create List         cat         dog
    ...     snake
    Log         ${list}

    #create dictionaty
    ${dict}         Create Dictionary       firstname=tanabodee     lastname=leelaprayul
    ...     animal=${list}
    Log         ${dict}

    #for in range
    FOR        ${index}        IN RANGE        5
        Log         ${index}
    END

    #for in list
    FOR         ${item}     IN      @{list}
        Log         ${item}
    END

    #for in dictionary
    FOR         ${item}     IN      &{dict}
        Log         ${item}
    END

Test Dictionary
    ${list}         Create List         cat         dog
    ...     snake
    ${child}         Create Dictionary         name=annie         age=8
    ${dict}         Create Dictionary       firstname=tanabodee     lastname=leelaprayul
    ...     child=${child}
    Log         ${dict['firstname']}
    Log         ${dict.firstname}
    Log         ${dict['child']['name']}

    ${length}       Get Length      ${list}
    FOR     ${index}      IN RANGE      ${length}
        Log         ${list[${index}]}
    END

Test Assertion
    ${dict}         Create Dictionary       firstname=tanabodee     lastname=leelaprayul
    Should Be Equal         TANABodee           ${dict['firstname']}        ignore_case=${TRUE}
    Should Be Equal         tanabodee            ${dict['firstname']}
    Should Be Equal As Strings         TANABodee           ${dict['firstname']}        ignore_case=${TRUE}

